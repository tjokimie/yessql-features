"use strict";

var _ = require('lodash')
  , color = require('cli-color')
  , SqlModel = require('yessql-core/models/SqlModel')
  , classUtils = require('yessql-core/class-utils')
  , multiRequire = require('yessql-core/utils/multi-require');

/**
 * Registers an express.js middleware that binds model classes to the request's database and stores them to the request.
 *
 * Example usage in config file:
 *
 * ```js
 * features: [
 *   ...
 *   {
 *     feature: 'database-bind-models',
 *     config: {
 *       modelPaths: [
 *         __dirname + '../models/*'
 *       ]
 *     }
 *   },
 *   ...
 * ]
 * ```
 *
 * The `SqlModel` subclasses are not bound to any database connection by default. One possibility
 * is to bind the model class each time it is used like so `SomeSqlModel.bindDb(req.db) ...` but
 * this is ugly. This feature binds all models to `req.db` automatically at the beginning of the
 * request and stores the bound models to `req.models`. Model classes in paths that match
 * `config.modelPaths` are automatically bound.
 *
 * @param {object} app
 *    express.js Application instance.
 *
 * @param {{modelPaths:Array.<String>}} config
 *    `modelPaths` is an array of path patterns from which the models are searched
 *    The patterns can be anything supported by node-glob.
 */
module.exports = function(app, config) {
  var modules = findModelClassModules(app, config);

  app.models = function () {
    // Throws if the database configuration is request specific.
    // See ConfigManager.knexConfig for more info.
    return bindModels(app.db());
  };

  app.use(function(req, res, next) {
    req.models = bindModels(req.db);
    next();
  });

  function bindModels(db) {
    var models = {};
    // Create a bound version of each model class by binding them to the given database.
    // This allows the model classes to be used with the correct database without passing
    // the database object to each method that performs database operations.
    for (var i = 0, l = modules.length; i < l; ++i) {
      var ModelClass = modules[i].module;
      models[ModelClass.name] = ModelClass.bindDb(db);
    }
    return models;
  }
};

/**
 * These features must be initialized before this one.
 *
 * @type {Array.<String>}
 */
module.exports.dependencies = ['database'];

/**
 * @returns {Array.<RequireResult>}
 * @private
 */
function findModelClassModules(app, config) {
  return _.reduce(config.modelPaths, function(allModules, modelPath) {
    var modules = multiRequire(modelPath)
      .filterModule(function(module) {
        return classUtils.isSubclassOf(module, SqlModel) && module !== SqlModel;
      })
      .require(function(module) {
        if (_.isEmpty(module.module.name)) {
          throw new Error(
            'invalid SqlModel in path ' +
            '"' + module.filePath + '": ' +
            'Constructor doesn\'t have a name.'
          );
        }
        if (app.config.profile !== 'testing') {
          logModelClassFound(module);
        }
      });
    return allModules.concat(modules);
  }, []);
}

/**
 * @param {RequireResult} module
 * @private
 */
function logModelClassFound(module) {
  console.log('  '
    + color.white('registering model class ')
    + color.cyan(module.fileName) + ' '
    + color.white('for database binding'));
}
